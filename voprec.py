from flask import Flask,render_template,request,session
from connection import con


app = Flask(__name__)
app.secret_key="hii"


@app.route('/')
def login():
    return render_template("login.html")
@app.route('/login_post',methods=["post"])
def login_post():
    username=request.form["textfield"]
    password=request.form["textfield2"]
    sel="select * from login where username='"+username+"' and password='"+password+"'"
    c=con()
    res=c.select_one(sel)
    if res is not None:
        if res[2]=="admin":
            return admin_home()
        else:
            sel2="select userid from signup where email_id='"+username+"'"
            res2=c.select_one(sel2)
            if res2[0] is None:
                session["userid"]=res2[0]
                return render_template("login.html")
            else:
                return user_home()
    else:
        return "NO"



@app.route('/admin_home')
def admin_home():
    return render_template("admin_home.html")
@app.route('/admin_citation')
def admin_citation():
    return render_template("admin_citation.html")
@app.route('/admin_doc_upload')
def admin_doc_upload():
    return render_template("admin_doc_upload.html")
@app.route('/admin_search_citation')
def admin_search_citation():
    return render_template("admin_search_citation.html")
@app.route('/user_signup')
def user_signup():
    return render_template("user_signup.html")
@app.route('/signup_post',methods=["post"])
def signup_post():
    image=request.files["fileField"]
    name=request.form["textfield"]
    gender=request.form["radio"]
    housename=request.form["textfield6"]
    post=request.form["textfield7"]
    pin_code=request.form["textfield8"]
    state=request.form["textfield9"]
    email_id=request.form["textfield2"]
    password=request.form["textfield4"]
    conf_password=request.form["textfield5"]
    phone_number=request.form["textfield3"]
    if password==conf_password:
        image.save("C:\\Users\\SUJI\\PycharmProjects\\voprec\\static\\user_images\\"+image.filename)
        path="/static/user_images/"+image.filename
        ins="insert into signup(name,gender,house_name,post,pin_code,state,email_id,phone_number,image)values('"+name+"','"+gender+"','"+housename+"','"+post+"','"+pin_code+"','"+state+"','"+email_id+"','"+phone_number+"','"+path+"')"
        c=con()
        r=c.non_return(ins)
        ins1="insert into login values('"+email_id+"','"+password+"','user')"
        r1=c.non_return(ins1)
        if r=="OK" and r1=="OK":
            return render_template("login.html")
        else:
            return "NO"
    else:
        return "NO"
@app.route('/user_search_citation')
def user_search_citation():
    return render_template("user_search_citation.html")
@app.route('/user_view_citation')
def user_view_citation():
    return render_template("user_view_citation.html")
@app.route('/user_home')
def user_home():
    return render_template("user_home.html")
@app.route('/user_profile_view')
def user_profile_view():
    return render_template("user_profile_view.html")
if __name__ == '__main__':
    app.run()
