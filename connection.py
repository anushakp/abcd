import pymysql
class con():
    def __init__(self):
        self.c=pymysql.connect(host="localhost",user="root",db="voprec",port=3306)
        self.cu=self.c.cursor()

    def non_return(self,que):
        self.res=self.cu.execute(que)
        self.c.commit()
        if self.res==1:
            return "OK"
        else:
            return "NO"
    def select_one(self,que):
        self.c = pymysql.connect(host="localhost", user="root", db="voprec", port=3306)
        self.cu = self.c.cursor()
        self.cu.execute(que)
        self.res=self.cu.fetchone()
        if self.res is not None:
            return self.res
        else:
            return "NO"
    def select_all(self,que):
        self.c = pymysql.connect(host="localhost", user="root", db="voprec", port=3306)
        self.cu = self.c.cursor()
        self.cu.execute(que)
        self.res=self.cu.fetchall()
        if self.res is not None:
            return self.res
        else:
            return "NO"
    def maxid(self,que):
        self.c = pymysql.connect(host="localhost", user="root", db="voprec", port=3306)
        self.cu = self.c.cursor()
        self.cu.execute(que)
        self.res=self.cu.fetchone()
        if self.res[0] is None:
            self.mid=1
        else:
            self.mid=int(self.res[0])+1
        return str(self.mid)